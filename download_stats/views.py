# Create your views here.
from django.http import Http404
from django.http import HttpResponse , HttpRequest

def download_file(request,filename=''):
    from .helpers import FileRegistry   
    file_registry = FileRegistry()
    #import ipdb ; ipdb.set_trace() 
    f = file_registry.get_file(request)
    if f.is_valid():
        #import ipdb ; ipdb.set_trace()
        return f.send_file()
    else:
        raise Http404


