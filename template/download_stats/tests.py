"""

Dada
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.utils import timezone
from models import DownloadFile, DownloadRecord
from download_stats.helpers import FileRegistry , FileWrapper
from django.http import HttpRequest , HttpResponse , Http404
from django.core.urlresolvers import reverse
from django.test.client import Client 
from django.conf import settings
import mock
import os
import tempfile
import datetime


class Download_StatsTest(TestCase):
    def create_tempfile(self,contents=''):
        f = tempfile.NamedTemporaryFile(delete=False)
        disk_path = f.name
        f.write(contents)
        f.close()
        return disk_path  
 
    def setUp(self):
        settings.SENDFILE_BACKEND = 'sendfile.backends.simple'
        self.url = '/download/temp.file'
        self.path = self.create_tempfile('this is a test')

        DownloadFile.objects.create(
                path = self.path,
                url = self.url
        )

    def test_valid(self):
        url = reverse('download_file', kwargs={'filename': 'temp.file'})
        response = self.client.get(url)
        content = response.content

        self.assertIsInstance(response,HttpResponse)
        self.assertEqual(response.status_code,200)
        self.assertEqual(content,'this is a test')

    def tearDown(self):
        os.unlink(self.path)

    def test_downloadfile_count(self):
        """
             checks the count of DownloadFile objects
        """
        count_obj = DownloadFile.objects.filter(path = self.path).count()
        self.assertEqual(count_obj,1)

    def test_downloadfile_hit_count(self):
        """
             checks the number of DownloadRecord created 
        """
        d = DownloadFile.objects.get(path = self.path)
        dr_obj = DownloadRecord()
        dr_obj.file = d
        dr_obj.save()
        count_obj = DownloadRecord.objects.filter(file=d).count()
        self.assertEqual(count_obj,1)

    def test_downloadfile_basename(self):
        """
              checks the basename of the DownloadFile
        """
        d = DownloadFile.objects.get(path = self.path)
        self.assertEqual(d.basename(),os.path.basename(self.path))

    def test_downloadfile_is_valid(self):
        """
               checks whether the download file exists on disk , is a file and readable
        """
        d_obj = DownloadFile.objects.get(path = self.path)
        self.assertEqual(d_obj.is_valid(),True)


    def test_downloadfile_not_valid(self):
        """
               checks for the condition downloadfile not valid
        """
        d_obj = DownloadFile.objects.get(path = self.path)
        d_obj.path= "/web/temp.file"
        self.assertEqual(d_obj.is_valid(),False)

    def test_FileWrapper_is_valid(self):
        """
            check whether a Filewrapper.file_obj exists and is_valid()
        """
        request = HttpRequest()
        f = FileWrapper(request=request)
        f.file_obj =  DownloadFile.objects.get(path = self.path )
        self.assertEqual(f.is_valid(),True)

    def test_FileWrapper_record_hit(self):
        """
             checks whether a FileWrapper creates a DownloadRecord 
        """
        r =  mock.Mock()
        r.user.email = mock.Mock()
        r.user.is_active = mock.Mock() 
        r.url = self.path 
        r.path = self.path
        r.META.REMOTE_ADDR  = mock.Mock()      
        f = FileWrapper(request=r)
        f.file_obj = DownloadFile.objects.get(path = self.path ) 
        self.assertIsInstance(f.record_hit(), DownloadRecord)
        #self.assertTrue(isinstance(f.record_hit(),DownloadRecord))

    def test_FileWrapper_send_file(self):
        """
            checks Filewrapper.send_file
        """
        settings.SENDFILE_BACKEND = 'sendfile.tests'
        from sendfile import sendfile
        request = HttpRequest()
        f_wrapper = FileWrapper(request=request)
        f_wrapper.file_obj = DownloadFile.objects.get( path = self.path)
        self.assertTrue(isinstance(f_wrapper.send_file(), HttpResponse))


    def test_FileRegistry_get_file(self):
        """
            chechs FileRegistry.get_File returns FileWrapper
        """
        request = HttpRequest()
        f_register = FileRegistry()
        self.assertTrue(isinstance(f_register.get_file(request),FileWrapper))


    def test_FileRegistry_register_file(self):
        """
             check whether a download file is created
        """
        f_register = FileRegistry()
        self.assertEqual(f_register.register_file(self.path,self.path,False),os.path.isfile(self.path) )
        
 




   


     

