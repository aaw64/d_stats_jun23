from django.conf.urls  import patterns,include,url
from django.views.generic import TemplateView 
import views as download_stats_views

urlpatterns = patterns(' ',
 url(
        r'(?P<filename>.+)$',
        download_stats_views.download_file,
        name='download_file',
    ),  
)
