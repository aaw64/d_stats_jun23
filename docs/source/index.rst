.. Download_stats documentation master file, created by
   sphinx-quickstart on Tue Jun 23 13:30:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django Download_stats's app documentation!
=====================================================



Introduction

django-download_stats is an reusable application for Django framework , that allows users to download files and also create a download record.

Installation

  Install django-download_stats by running:

  startapp.sh django-download_stats


Running the tests

  python setup.py test

Requirements:

   django >= 1.4


Contents:

.. toctree::
   :maxdepth: 5

   Getting-started
   Testing
   Contributing
   





