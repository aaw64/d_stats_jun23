Getting Started Guide
=====================

Installation
----------------

pip install https://bitbucket.org/unh-gislab/download_stats_1.0/get/v1.0.zip

Source code can be downloaded from https://bitbucket.org/unh-gislab/download_stats_1.0

Run startapp.sh **download_stats** to start the app

Configuration
----------------

 * Add to the INSTALLED_APPS in settings.py:

     *download_stats*


 Edit config.py and setup.py for more advanced configuration

How django-download_stats works

::

    def download_file(request,filename=''):
        from .helpers import FileRegistry   
        file_registry = FileRegistry() 
        f = file_registry.get_file(request)
        if f.is_valid():
            return f.send_file()
        else:
            raise Http404 


