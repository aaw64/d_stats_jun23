#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

if [ $# -eq 0 ]
  then
    echo "You must provide an app name"
  else
    cd $DIR
    cp -r template $1
    find $1/ -type f -exec sed -i 's/{{app_name}}/'$1'/g' "{}" +;
    echo 'New app created in '$DIR'/'$1
fi
